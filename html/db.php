<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ข้อมูลนักศึกษา</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            text-align: center;
            margin: 50px;
            background-color: #808080;
        }
        h1 {
            color: #000000;
        }
        .info-container {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            height: 80vh;
        }
        .info-box {
            border: 2px solid #000000;
            border-radius: 10px;
            padding: 20px;
            background-color: #ffffff;
            width: 300px;
            margin-bottom: 20px;
        }
        img {
            border-radius: 50%;
            margin-top: 20px;
        }
        p {
            font-size: 18px;
            margin: 10px 0;
        }
        strong {
            color: #257256;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            font-size: 16px;
            text-decoration: none;
            background-color: #000000;
            color: #ffffff;
            border-radius: 5px;
            margin-top: 15px;
            cursor: pointer;
            margin-right: 10px; 
        }
    </style>
</head>
<body>

    <div class="info-container">
        <div class="info-box">
        <?php
   $servername = "db";
   $username = "devops";
   $password = "devops101";

   $dbhandle = mysqli_connect($servername, $username, $password);
   $selected = mysqli_select_db($dbhandle, "titanic");

   echo "Connected database server<br>";
   echo "Selected database";
?>
            <a href="index.html" class="tab">Profile</a>
            <a href="interested.html" class="btn">สิ่งที่ฉันสนใจ</a>
            <a href="about_su.html" class="btn">รอบรั้วมหาวิทยาลัยศิลปากร</a>
        </div>
    </div>

</body>
</html>
